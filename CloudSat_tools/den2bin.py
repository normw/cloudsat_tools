#!/usr/bin/env python3

""" Methods for working with CloudSat bit field variables.
"""

#--------------------------------------------------------------------------
# Methods for working with CloudSat bit-field variables
#
# Copyright (c) 2015 Norman Wood
#
# This file is part of the free software CloudSat_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------

import numpy

def tcbin(x, y=8):
   """
   Modified from http://www.barricane.com/bit-twiddling-python.html which was
   posted 05 October 2008 by Chris Dew (cmsdew@gmail.com)
   This function returns the padded, two's complement representation of x, in y bits.
   Overcomes limitations in python 2.6 bin() function - bin() shows signed
   bit fields rather than twos-complement versions.

	y is the number of bits to be used in the representation of x, and can take
   any non-zero positive value, but takes values of 8 (2^3), 16 (2^4), 32 (2^5),
   or 64 (2^6) typically, corresponding to traditional character, integer,
   long integer, and long long integer bit lengths.
   """
   if x >= (2**(y - 1)) or x < -(2**(y - 1)):
      raise ValueError("Argument out of range for %d bits" %(y,))
   if y < 1:
      raise ValueError("Representation must use more than %d bits" %(y,))
   if x >= 0:
      bitstring = bin(int(x))
      #Pad with leading zeros
      while len(bitstring) < y+2:
         bitstring = "0b0" + bitstring[2:] 
   else:
      bitstring = bin(2**y + x)  #This should have the correct number of digits after the "b" without padding
   return(bitstring)

def bitfield(bitstring):
   #I want the MSB at the largest index, LSB at the zero index so reverse the bitstring
   bitlist = [1 if digit=='1' else 0 for digit in reversed(bitstring[2:])]
   bitfield = numpy.array(bitlist, dtype=int)
   return bitfield

def tostring(flag_array):
   return "".join(flag_array.astype('S'))

def myDenary2Binary(n, bits=8):
   bit_array = numpy.zeros((bits,),dtype=int)
   if n < 0:
      print(n)
      raise ValueError("Must be a positive integer")
   if n == 0:  return bit_array
   index = 0
   while n > 0:
      bit_array[index] = n % 2
      n = n >> 1
      index += 1
   return bit_array

def Denary2Binary(n):
   '''convert denary integer n to binary string bStr'''
   bStr = ''
   if n < 0: raise ValueError("must be a positive integer")
   if n == 0: return '0'
   while n > 0:
      bStr = str(n % 2) + bStr
      n = n >> 1
   return bStr
 
def int2bin(n, count=24):
   """returns the binary of integer n, using count number of digits"""
   return "".join([str((n >> y) & 1) for y in range(count-1, -1, -1)])
 
if __name__ == '__main__':
   import sys
   #print Denary2Binary(255) # 11111111
 
   ## convert back to test it
   #print int(Denary2Binary(255), 2) # 255
 
   #print

   #print "my bit array"
   #result = myDenary2Binary(255)
   #for i_bit in range(len(result)-1,-1,-1):
   #   print "%1d" %(result[i_bit]),
   #print

 
   ## this version formats the binary
   #print int2bin(255, 16) # 000011111111
   ## test it
   #print int("000011111111", 2) # 255
 
   #print
 
   ## check the exceptions
   #print Denary2Binary(0)
   #print Denary2Binary(-5) # should give a ValueError

   print(tcbin(int(sys.argv[1])))
   print(myDenary2Binary(int(sys.argv[1])))

#!/usr/bin/env /usr/bin/python

#Go through a directory of cloudsat granules and sample specified fields
#within a particular distance of a specified lat and lon

import sys
import os
import string
import numpy
import datetime

import h5py

#Module data
CloudSat_file = None
f_cloudsat_locs = None
cloudsat_granule_idx = None
cloudsat_lat_rads = None
cloudsat_lon_rads = None
cloudsat_year = None
cloudsat_doy = None
N_granules = None
N_profiles = None


def init(cloudsat_locs_file_name):
   global CloudSat_file, f_cloudsat_locs, cloudsat_granule_idx, cloudsat_lat_rads, cloudsat_lon_rads, cloudsat_year, cloudsat_doy, N_granules, N_profiles
   CloudSat_file = cloudsat_locs_file_name
   f_cloudsat_locs = h5py.File(CloudSat_file, 'r')
   cloudsat_granule_idx = f_cloudsat_locs['granule_idx']
   cloudsat_lat_rads = f_cloudsat_locs['latitude']   #Already in radians
   cloudsat_lon_rads = f_cloudsat_locs['longitude']  #Already in radians
   cloudsat_year = f_cloudsat_locs['year']
   cloudsat_doy = f_cloudsat_locs['day_of_year']
   N_granules = cloudsat_lat_rads.shape[0]
   N_profiles = cloudsat_lat_rads.shape[1]


def distance(phi_s, lambda_s, phi_f, lambda_f):
   #The central angle formula is from the Wikipedia page for great circle distance,
   #and is the Vincenty formula applied to a sphere
   #phi_s, lambda_s are the lat and lon of the starting points, radians
   #phi_f, lambda_f are the lat and lon of the ending points, radians

   Earth_radius = 6.37101e3     #kilometers
   delta_lambda = lambda_f - lambda_s
   delta_phi = phi_f - phi_s
   num_term1 = numpy.power(numpy.cos(phi_f)*numpy.sin(delta_lambda),2)
   num_term2 = numpy.power(numpy.cos(phi_s)*numpy.sin(phi_f) - numpy.sin(phi_s)*numpy.cos(phi_f)*numpy.cos(delta_lambda),2)
   num = numpy.sqrt(num_term1 + num_term2)
   denom = numpy.sin(phi_s)*numpy.sin(phi_f) + numpy.cos(phi_s)*numpy.cos(phi_f)*numpy.cos(delta_lambda)
   central_angle = numpy.arctan2(num,denom)
   distance = Earth_radius*central_angle
   return distance

def granule(file):
   #Get the granule number from the filename
   path_parts = string.splitfields(file, os.sep)
   filename = path_parts[-1]
   filename_parts = string.splitfields(filename, "_")
   granule_label = filename_parts[1]
   granule = int(granule_label)
   return granule
   
def collocations(target_lat_deg, target_lon_deg, threshold_km):
   global CloudSat_file
   deg_to_rad = 2.*numpy.pi/360.
   results = []
   config_string = "# Lat: %10.4f Lon: %10.4f Distance_thresh: %10.4f Locations file: %s" %(target_lat_deg, target_lon_deg, threshold_km, CloudSat_file)
   results.append(config_string)
   header_string = "#Granule Year DOY Count Index Distance   Subsat lat  Subsat lon"
   results.append(header_string)

   granule_count = 0
   for i_granule in range(N_granules):
      granule_label = "%06d" %(cloudsat_granule_idx[i_granule])
      year = cloudsat_year[i_granule]
      doy = cloudsat_doy[i_granule]
      the_distances = distance(target_lat_deg*deg_to_rad, target_lon_deg*deg_to_rad, cloudsat_lat_rads[i_granule,:], cloudsat_lon_rads[i_granule,:])
      mask = numpy.less(the_distances, threshold_km)
      indices = numpy.nonzero(mask)
      count = len(indices[0])
      if count > 0:
         granule_count += 1
         for idx in indices[0]:
            results_string = " %6s %4d %3d %4d %6d %8.3f %10.4f %10.4f" %(granule_label, year, doy, count, idx, the_distances[idx], cloudsat_lat_rads[i_granule,idx]/deg_to_rad, cloudsat_lon_rads[i_granule,idx]/deg_to_rad)
            results.append(results_string)
   if granule_count > 0:
      results.append("#Granule count %d" %(granule_count,))
      return(results)
   else:
      return None

def collocations_alt(target_lat_deg, target_lon_deg, threshold_km, start_datetime, end_datetime):
   #Includes time and space constraints
   global CloudSat_file
   deg_to_rad = 2.*numpy.pi/360.
   results = []
   config_string = "# Lat: %10.4f Lon: %10.4f Distance_thresh: %10.4f Locations file: %s" %(target_lat_deg, target_lon_deg, threshold_km, CloudSat_file)
   results.append(config_string)
   header_string = "#Granule Year DOY Count Index Distance   Subsat lat  Subsat lon"
   results.append(header_string)

   granule_count = 0
   for i_granule in range(N_granules):
      granule_label = "%06d" %(cloudsat_granule_idx[i_granule])
      year = cloudsat_year[i_granule]
      doy = cloudsat_doy[i_granule]
      timestring = "%04d-%03d" %(year, doy)
      granule_time = datetime.datetime.strptime(timestring,'%Y-%j')
      if start_datetime < granule_time and granule_time < end_datetime:
         the_distances = distance(target_lat_deg*deg_to_rad, target_lon_deg*deg_to_rad, cloudsat_lat_rads[i_granule,:], cloudsat_lon_rads[i_granule,:])
         mask = numpy.less(the_distances, threshold_km)
         indices = numpy.nonzero(mask)
         count = len(indices[0])
         if count > 0:   
            granule_count += 1
            for idx in indices[0]:
               results_string = " %6s %4d %3d %4d %6d %8.3f %10.4f %10.4f" %(granule_label, year, doy, count, idx, the_distances[idx], cloudsat_lat_rads[i_granule,idx]/deg_to_rad, cloudsat_lon_rads[i_granule,idx]/deg_to_rad)
               results.append(results_string)
   if granule_count > 0:
      results.append("#Granule count %d" %(granule_count,))
      return(results)
   else:
      return(None)

def collocations_latlon_box(target_lat_deg_start, target_lat_deg_end, target_lon_deg_start, target_lon_deg_end):
   global CloudSat_file, N_granules
   deg_to_rad = 2.*numpy.pi/360.
   results = []
   config_string = "# Lat: %10.4f to %10.4f Lon: %10.4f to %10.4f Locations file: %s" %(target_lat_deg_start, target_lat_deg_end, target_lon_deg_start, target_lon_deg_end, CloudSat_file)
   granule_count = 0
   for i_granule in range(N_granules):
      granule_label = "%06d" %(cloudsat_granule_idx[i_granule])
      year = cloudsat_year[i_granule]
      doy = cloudsat_doy[i_granule]
      timestring = "%04d-%03d" %(year, doy)
      granule_time = datetime.datetime.strptime(timestring,'%Y-%j')
      mask_1 = numpy.less(target_lat_deg_start*deg_to_rad, cloudsat_lat_rads[i_granule,:])
      mask_2 = numpy.less(cloudsat_lat_rads[i_granule,:], target_lat_deg_end*deg_to_rad)
      mask_3 = numpy.less(target_lon_deg_start*deg_to_rad, cloudsat_lon_rads[i_granule,:])
      mask_4 = numpy.less(cloudsat_lon_rads[i_granule,:], target_lon_deg_end*deg_to_rad)

      indices = numpy.nonzero(numpy.logical_and(mask_1, numpy.logical_and(mask_2, numpy.logical_and(mask_3, mask_4))))
      count = len(indices[0])
      if count > 0:
         granule_count += 1
         for idx in indices[0]:
            results_string = " %6s %4d %3d %4d %6d %10.4f %10.4f" %(granule_label, year, doy, count, idx, cloudsat_lat_rads[i_granule,idx]/deg_to_rad, cloudsat_lon_rads[i_granule,idx]/deg_to_rad)
            #print results_string
            results.append(results_string)
   if granule_count > 0:
      results.append("#Granule count %d" %(granule_count,))
      return(results)
   else:
      return(None)
   

def collocations_summary(target_lat_deg, target_lon_deg, threshold_km, start_datetime, end_datetime):
   #Includes time and space constraints
   global CloudSat_file
   deg_to_rad = 2.*numpy.pi/360.
   results = []
   config_string = "# Lat: %10.4f Lon: %10.4f Distance_thresh: %10.4f Locations file: %s" %(target_lat_deg, target_lon_deg, threshold_km, CloudSat_file)
   results.append(config_string)
   header_string = "#Granule Year DOY Count Index Distance   Subsat lat  Subsat lon"
   results.append(header_string)

   granule_count = 0
   for i_granule in range(N_granules):
      granule_label = "%06d" %(cloudsat_granule_idx[i_granule])
      year = cloudsat_year[i_granule]
      doy = cloudsat_doy[i_granule]
      timestring = "%04d-%03d" %(year, doy)
      granule_time = datetime.datetime.strptime(timestring,'%Y-%j')
      if start_datetime < granule_time and granule_time < end_datetime:
         the_distances = distance(target_lat_deg*deg_to_rad, target_lon_deg*deg_to_rad, cloudsat_lat_rads[i_granule,:], cloudsat_lon_rads[i_granule,:])
         mask = numpy.less(the_distances, threshold_km)
         indices = numpy.nonzero(mask)
         count = len(indices[0])
         if count > 0:   
            granule_count += 1
            results_string = " %6s %4d %2d %2d %6d" %(granule_label, granule_time.year, granule_time.month, granule_time.day, count)
            results.append(results_string)
   if granule_count > 0:
      return(results)
   else:
      return(None)


if __name__ == "__main__":
   init("cloudsat_lat_lon_time_E01234.hdf5")
   print "Init done, getting collocations"
   #results = collocations(45.0, -105.0, 25.)
   results = collocations_latlon_box(41., 44., -110.0, -100.)
   print "Got collocations"
   if results != None:
      for result in results:
         data = result.split()
         granule_label = copy.deepcopy(data[0])
         if granule_label != granule_label_current:
            #New granule label
            print granule_label
            granule_label_current = copy.deepcopy(granule_label)
         #print "%s" % (result,)

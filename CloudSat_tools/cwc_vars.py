#!/usr/bin/env python

""" Example of a file containing metadata to help with plotting and
analysis of CloudSat variables.  Each dictionary entry contains:
   * the CloudSat variable name
   * the variable's scaling factor
   * the variable's offset
   * the variable's minimum value
   * the variable's maximum value

This was used before the read_var* modules were set up, in which the various
read_var_* routines would read the variable's attributes to get the scale
factor and offset.  The typical min/max values are useful for setting
plot ranges to reasonable values.
"""

#--------------------------------------------------------------------------
# Metadata for CloudSat 2B-CWC-RO and 2B-CWC-RVOD variables.
#
# Copyright (c) 2015 Norman Wood
#
# This file is part of the free software CloudSat_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------

   

vars_RVOD = {\
           'RVOD_ice_effective_radius': ('ice_eff_rad', 10., 0., 0., 1000.), \
           'RVOD_ice_number_conc': ('ice_number_conc', 10., 0., 0., 5000.),  \
           'RVOD_ice_phase_fraction': ('ice_phase_frac', 1000., 0., 0., 1.), \
           'RVOD_ice_water_content': ('ice_water_content', 1., 0., 0., 35000.), \
           'RVOD_ice_distrib_width_param': ('ice_width_param', 1000., 0., 0., 3.), \
           'RVOD_liq_effective_radius': ('liq_eff_rad', 10., 0., 0., 100.), \
           'RVOD_liq_number_conc': ('liq_number_conc', 10., 0., 0., 300.),  \
           'RVOD_liq_water_content': ('liq_water_content', 1., 0., 0., 35000.), \
           'RVOD_liq_distrib_width_param': ('liq_width_param', 1000., 0., 0., 3.), \
           'LO_RVOD_norm_chi_square': ('liq_norm_chi_sq', 100., 0., 0., 5000.),\
           'IO_RVOD_norm_chi_square': ('ice_norm_chi_sq', 100., 0., 0., 5000.)}

uncerts_RVOD = {\
           'RVOD_ice_effective_radius_uncertainty': ('ice_eff_rad_uncert', 1., 0., 0., 250.), \
           'RVOD_ice_number_conc_uncertainty': ('ice_number_conc_uncert', 1., 0., 0., 250.),  \
           'RVOD_ice_water_content_uncertainty': ('ice_water_content_uncert', 1., 0., 0., 250.), \
           'RVOD_ice_distrib_width_param_uncertainty': ('ice_width_param_uncert', 1., 0., 0., 250.), \
           'RVOD_liq_effective_radius_uncertainty': ('liq_eff_rad_uncert', 1., 0., 0., 250.), \
           'RVOD_liq_number_conc_uncertainty': ('liq_number_conc_uncert', 1., 0., 0., 250.),  \
           'RVOD_liq_water_content_uncertainty': ('liq_water_content_uncert', 1., 0., 0., 250.), \
           'RVOD_liq_distrib_width_param_uncertainty': ('liq_width_param_uncert', 1., 0., 0., 250.)}

vars_RO = {\
           'RO_ice_effective_radius': ('ice_eff_rad', 10., 0., 0., 1000.), \
           'RO_ice_number_conc': ('ice_number_conc', 10., 0., 0., 5000.),  \
           'RO_ice_phase_fraction': ('ice_phase_frac', 1000., 0., 0., 1.), \
           'RO_ice_water_content': ('ice_water_content', 1., 0., 0., 35000.), \
           'RO_ice_distrib_width_param': ('ice_width_param', 1000., 0., 0., 3.), \
           'RO_liq_effective_radius': ('liq_eff_rad', 10., 0., 0., 100.), \
           'RO_liq_number_conc': ('liq_number_conc', 10., 0., 0., 300.),  \
           'RO_liq_water_content': ('liq_water_content', 1., 0., 0., 35000.), \
           'RO_liq_distrib_width_param': ('liq_width_param', 1000., 0., 0., 3.), \
           'LO_RO_norm_chi_square': ('liq_norm_chi_sq', 100., 0., 0., 5000.),\
           'IO_RO_norm_chi_square': ('ice_norm_chi_sq', 100., 0., 0., 5000.)}

uncerts_RO = {\
           'RO_ice_effective_radius_uncertainty': ('ice_eff_rad_uncert', 1., 0., 0., 250.), \
           'RO_ice_number_conc_uncertainty': ('ice_number_conc_uncert', 1., 0., 0., 250.),  \
           'RO_ice_water_content_uncertainty': ('ice_water_content_uncert', 1., 0., 0., 250.), \
           'RO_ice_distrib_width_param_uncertainty': ('ice_width_param_uncert', 1., 0., 0., 250.), \
           'RO_liq_effective_radius_uncertainty': ('liq_eff_rad_uncert', 1., 0., 0., 250.), \
           'RO_liq_number_conc_uncertainty': ('liq_number_conc_uncert', 1., 0., 0., 250.),  \
           'RO_liq_water_content_uncertainty': ('liq_water_content_uncert', 1., 0., 0., 250.), \
           'RO_liq_distrib_width_param_uncertainty': ('liq_width_param_uncert', 1., 0., 0., 250.)}

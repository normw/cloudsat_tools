#!/usr/bin/env python

""" Routines for extracting information from CloudSat file names and for
selecting CloudSat files based on year and day of year (doy).
"""

#--------------------------------------------------------------------------
# Methods for extraction information and selecting CloudSat files
#
# Copyright (c) 2015 Norman Wood
#
# This file is part of the free software CloudSat_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import os
import datetime

def get_granule(fpath):
   (path, fname) = os.path.split(fpath)
   parts = fname.split("_")
   granule_string = parts[1]
   return granule_string


def select_by_doy(year_list, doy_start_list, doy_end_list, fpath_list):
   new_fpath_list = []
   for fpath in fpath_list:
      (path, fname) = os.path.split(fpath)
      parts = fname.split("_")
      datestring = parts[0]
      file_year = int(datestring[0:4])
      file_doy = int(datestring[4:7])

      #Get indices that match file_year
      year_match_idxs = [i for i,x in enumerate(year_list) if x == file_year]
      for idx in year_match_idxs:
         doy_start = doy_start_list[idx]
         doy_end = doy_end_list[idx]
         if doy_start <= file_doy and file_doy <= doy_end:
            new_fpath_list.append(fpath)
   return new_fpath_list

      
def get_datetime(fpath):
   datetime_format = "%Y%j%H%M%S"
   (path, fname) = os.path.split(fpath)
   parts = fname.split("_")
   datetime_string = parts[0]
   datetime_val = datetime.datetime.strptime(datetime_string, datetime_format)
   return datetime_val



def test():

    fpath = '2017155032836_59053_CS_ECMWF-AUX_GRANULE_P_R04_E06.hdf'
    fpath_datetime = get_datetime(fpath)
    print(fpath_datetime)

if __name__ == "__main__":
    test()

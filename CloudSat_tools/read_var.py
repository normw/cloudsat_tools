#!/usr/bin/env python

""" Methods for reading 0D (scalar), 1D (vector) and 2D (array) variables 
from CloudSat data files.  These "read_var" methods were used prior to 
CloudSat files being structured to be fully HDF-EOS compliant and are kept
around to support legacy python scripts.
"""

#--------------------------------------------------------------------------
# Methods for reading variables from CloudSat files.
#
# Copyright (c) 2015 Norman Wood
#
# This file is part of the free software CloudSat_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------



"""
Examples of use:

Scalar variable
###############

import pyhdf.HDF
import CloudSat_tools.read_var

f_ptr = pyhdf.HDF.HDF(filename, pyhdf.HDF.HC.READ)
var = CloudSat_tools.read_var.get_0D_var(f_ptr, varname)


1D variable
###########

import pyhdf.HDF
import CloudSat_tools.read_var

f_ptr = pyhdf.HDF.HDF(filename, pyhdf.HDF.HC.READ)
var = CloudSat_tools.read_var.get_1D_var(f_ptr, varname)

This can be used to read 1D variables (like 'Latitude'
and 'Longitude') and also attributes (like 'Latitude.factor')

2D variable
###########

import pyhdf.SD
import CloudSat_tools.read_var

f_ptr = pyhdf.SD.SD(filename, pyhdf.SD.SDC.READ)
var = CloudSat_tools.read_var.get_2D_var(f_ptr, varname)

"""

import numpy
import warnings

with warnings.catch_warnings():
   warnings.filterwarnings("ignore",category=DeprecationWarning)
   import pyhdf.HDF
   import pyhdf.VS
   import pyhdf.SD

def get_0D_var(file_VS_ptr, varname):
   vs = file_VS_ptr.vstart()
   var = vs.attach(varname)
   tmp = var.read(1)
   var_value = tmp[0][0]
   return(var_value)

def get_1D_var(file_VS_ptr, varname):
   vs = file_VS_ptr.vstart()
   var = vs.attach(varname)
   var_info = var.inquire()
   var_nRecs = var_info[0]
   tmp = var.read(var_nRecs)
   var_values = numpy.array(tmp)
   var.detach()
   return(var_values)

def get_2D_var(file_SD_ptr, varname, scale_default = None, offset_default = None):
   with warnings.catch_warnings():
      warnings.filterwarnings("ignore",category=DeprecationWarning)
      var = file_SD_ptr.select(varname)
      try:
         scale_factor = var.attributes()['factor']
      except KeyError:
         if scale_default != None:
            scale_factor = scale_default
         else:
            scale_factor = 1.
      try:
         offset = var.attributes()['offset']
      except KeyError:
         if offset_default != None:
            offset = offset_default
         else:
            offset = 0.
      var_values = (var[:]-offset)/scale_factor
      return var_values

def secs_to_HHMMSS(utc_seconds):
   int_utc_seconds = utc_seconds.astype('int32')
   hh = int_utc_seconds/3600
   int_utc_seconds_remainder = int_utc_seconds - 3600*hh
   mm = int_utc_seconds_remainder/60
   int_utc_seconds_remainder = int_utc_seconds_remainder - 60*mm
   ss = int_utc_seconds_remainder
   return (hh,mm,ss)
